﻿ USE practica3;

-- 1- Visualizar el número de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamento. 

  SELECT e.dept_no, COUNT(*)empleados 
    FROM emple e 
    GROUP BY e.dept_no;

-- 2. Visualizar los departamentos con más de 5 empleados. Utilizar GROUP BY para agrupar por departamento y HAVING para establecer la condición sobre los grupos. 

  SELECT e.dept_no, COUNT(*)empleados 
    FROM emple e 
    GROUP BY e.dept_no 
    HAVING empleados>5;

-- 3. Hallar la media de los salarios de cada departamento (utilizar la función avg y GROUP BY). 

  SELECT e.dept_no, AVG(e.salario)salario_medio 
    FROM emple e  
    GROUP BY e.dept_no;

-- 4. Visualizar el nombre de los empleados vendedores del departamento ʻVENTASʼ (Nombre del departamento=ʼVENTASʼ, oficio=ʼVENDEDORʼ). 

  -- Subconsulta C1
  -- Numero de departamento ventas
  SELECT d.dept_no 
    FROM depart d 
    WHERE d.dnombre='VENTAS';

  -- Consulta final 
  SELECT e.apellido 
    FROM emple e 
    WHERE e.oficio='VENDEDOR' 
    AND e.dept_no=(
      SELECT d.dept_no 
        FROM depart d 
        WHERE d.dnombre='VENTAS'); 

-- 5. Visualizar el número de vendedores del departamento ʻVENTASʼ (utilizar la función COUNT sobre la consulta anterior). 

  -- Subconsulta C1
  -- Numero de departamento ventas
  SELECT d.dept_no 
    FROM depart d 
    WHERE d.dnombre='VENTAS';

  -- Subconsulta C2
  -- nombre de los empleados vendedores 
  SELECT e.apellido 
    FROM emple e 
    WHERE e.oficio='VENDEDOR' 
    AND e.dept_no=(
      SELECT d.dept_no 
        FROM depart d 
        WHERE d.dnombre='VENTAS');  
  
  -- Consulta final
  SELECT COUNT(*)vendedores 
    FROM (
      SELECT e.apellido 
        FROM emple e 
        WHERE e.oficio='VENDEDOR' 
        AND e.dept_no=(
          SELECT d.dept_no 
            FROM depart d 
            WHERE d.dnombre='VENTAS'))C1; 

-- 6. Visualizar los oficios de los empleados del departamento ʻVENTASʼ. 

  -- Subconsulta C1 
  -- Numero de departamento ventas
  SELECT d.dept_no 
    FROM depart d 
    WHERE d.dnombre='VENTAS';

  -- Consulta final
  SELECT DISTINCT e.oficio 
    FROM emple e 
      WHERE e.dept_no = (
        SELECT d.dept_no 
          FROM depart d 
          WHERE d.dnombre='VENTAS');

-- 7. A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento cuyo oficio sea ʻEMPLEADOʼ (utilizar GROUP BY para agrupar por departamento. En la cláusula WHERE habrá que indicar que el oficio es ʻEMPLEADOʼ). 

  SELECT e.dept_no, COUNT(*)empleados 
    FROM emple e 
    WHERE e.oficio='EMPLEADO' 
    GROUP BY e.dept_no;

-- 8. Visualizar el departamento con más empleados.
  
  -- Subconsulta C1
  -- Empleados por departamento
  SELECT e.dept_no, COUNT(*)empleados 
    FROM emple e 
    GROUP BY e.dept_no;

  -- Subconsulta C2
  -- Numero maximo de empleados en un departamento
  SELECT MAX(C1.empleados)maximo 
    FROM (
      SELECT e.dept_no, COUNT(*)empleados 
        FROM emple e 
        GROUP BY e.dept_no) C1;

  -- Consulta final
  SELECT C1.dept_no 
    FROM (
      SELECT e.dept_no, COUNT(*)empleados 
        FROM emple e 
        GROUP BY e.dept_no
    ) C1 
    JOIN (
      SELECT MAX(C1.empleados)maximo 
        FROM (
          SELECT e.dept_no, COUNT(*)empleados 
            FROM emple e 
            GROUP BY e.dept_no
        ) C1
      ) C2 
      ON C1.empleados = C2.maximo;

-- 9. Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados. 

  -- Subconsulta C1
  -- Media de salarios 
  SELECT AVG(salario) mediaSalarios 
    FROM emple e;

  -- Subconsulta C2
  -- Suma de salarios por departameno
  SELECT e.dept_no, SUM(e.salario) sumaSalarios 
    FROM emple e 
    GROUP BY e.dept_no;

  -- Consulta final
  SELECT C2.dept_no 
    FROM (
      SELECT AVG(salario) mediaSalarios 
        FROM emple e
    ) C1 
    JOIN (
      SELECT e.dept_no, SUM(e.salario) sumaSalarios 
        FROM emple e 
        GROUP BY e.dept_no
    ) C2 
    ON C2.sumaSalarios > C1.mediaSalarios;

-- 10. Para cada oficio obtener la suma de salarios. 

  SELECT e.oficio, SUM(salario)sumaSalrios 
    FROM emple e 
    GROUP BY e.oficio;

-- 11. Visualizar la suma de salarios de cada oficio del departamento ʻVENTASʼ. 
  
  -- Subconsulta C1 
  -- Numero de departamento ventas
  SELECT d.dept_no 
    FROM depart d 
    WHERE d.dnombre='VENTAS';

  -- Consulta final
  SELECT e.oficio, SUM(e.salario) sumaSalarios 
    FROM emple e 
      WHERE e.dept_no = (
        SELECT d.dept_no 
        FROM depart d 
        WHERE d.dnombre='VENTAS'
      ) 
    GROUP BY e.oficio;

-- 12. Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado. 

  -- Subconsulta C1
  -- Empleados que hay en cada departamento
  SELECT e.dept_no, COUNT(*) empleados 
    FROM emple e 
    WHERE e.oficio='EMPLEADO' 
    GROUP BY e.dept_no;

  -- Subconsulta C2
  -- Numero maximo de empleados qeu hay en un departamento
  SELECT MAX(C1.empleados)maximo 
    FROM (
      SELECT e.dept_no, COUNT(*) empleados 
        FROM emple e 
        WHERE e.oficio='EMPLEADO' 
        GROUP BY e.dept_no
    ) C1;

  -- Consulta final
  SELECT C1.dept_no 
    FROM (
      SELECT e.dept_no, COUNT(*) empleados 
        FROM emple e 
        WHERE e.oficio='EMPLEADO' 
        GROUP BY e.dept_no
    ) C1 
    JOIN (
      SELECT MAX(C1.empleados)maximo 
        FROM (
          SELECT e.dept_no, COUNT(*) empleados 
            FROM emple e 
            WHERE e.oficio='EMPLEADO' 
            GROUP BY e.dept_no
        ) C1
    ) C2 
    ON C1.empleados = C2.maximo;

-- 13. Mostrar el número de oficios distintos de cada departamento. 

  SELECT e.dept_no, COUNT(DISTINCT e.oficio) numOficios 
    FROM emple e 
    GROUP BY e.dept_no;

-- 14. Mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión. 

  -- Subconsulta C1 
  -- Departamentos y oficios que tienes mas de dos empleados
  SELECT e.dept_no, e.oficio, COUNT(*)numEmpleados 
        FROM emple e 
        GROUP BY e.dept_no, e.oficio
        HAVING numEmpleados>2;

  -- Consulta final
  SELECT C1.dept_no 
    FROM (
      SELECT e.dept_no, e.oficio, COUNT(*)numEmpleados 
        FROM emple e 
        GROUP BY e.dept_no, e.oficio
        HAVING numEmpleados>2
    ) C1;

-- 15. Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades.

  SELECT h.estanteria, SUM(h.unidades)unidades 
    FROM herramientas h 
    GROUP BY h.estanteria; 

-- 16. Visualizar la estantería con más unidades de la tabla HERRAMIENTAS. (con totales y sin totales) 

  -- CON TOTALES
  
  -- Subconcsulta C1
  -- Unidades por cada estanteria
  SELECT h.estanteria, SUM(h.unidades)unidades 
    FROM herramientas h 
    GROUP BY h.estanteria;

  -- Subconsulta C2
  -- Maximo de unidades en una estanteria
  SELECT MAX(C1.unidades)maximo 
    FROM (
      SELECT h.estanteria, SUM(h.unidades)unidades 
        FROM herramientas h 
        GROUP BY h.estanteria
    ) C1;

  -- Consulta final
  SELECT C1.estanteria 
    FROM (
      SELECT h.estanteria, SUM(h.unidades)unidades 
        FROM herramientas h 
        GROUP BY h.estanteria
    ) C1 
    JOIN (
      SELECT MAX(C1.unidades)maximo 
        FROM (
          SELECT h.estanteria, SUM(h.unidades)unidades 
            FROM herramientas h 
            GROUP BY h.estanteria
        ) C1
    ) C2 
    ON C1.unidades = C2.maximo;

-- 17. Mostrar el número de médicos que pertenecen a cada hospital, ordenado por número descendente de hospital. 

  SELECT m.cod_hospital, COUNT(*)numMedicos 
    FROM medicos m 
    GROUP BY m.cod_hospital
    ORDER BY m.cod_hospital DESC;

-- 18. Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene. 

  SELECT DISTINCT m.cod_hospital, m.especialidad 
    FROM medicos m;

-- 19. Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos (tendrás que partir de la consulta anterior y utilizar GROUP BY). 

  SELECT m.cod_hospital, m.especialidad, COUNT(*) numMedicos 
    FROM medicos m
    GROUP BY m.cod_hospital, m.especialidad;

-- 20. Obtener por cada hospital el número de empleados. 

  SELECT p.cod_hospital, COUNT(*)empleados 
    FROM personas p 
    GROUP BY p.cod_hospital;

-- 21. Obtener por cada especialidad el número de trabajadores. 

  SELECT m.especialidad, COUNT(*)medicos 
    FROM medicos m 
    GROUP BY m.especialidad;

-- 22. Visualizar la especialidad que tenga más médicos. 

  -- Subconsulta C1
  SELECT especialidad, COUNT(*)empleados 
    FROM medicos 
    GROUP BY especialidad;

  -- Subconsulta C2
  SELECT MAX(C1.empleados) maximo 
    FROM (
      SELECT especialidad, COUNT(*)empleados 
        FROM medicos 
        GROUP BY especialidad
    ) C1;

  -- Consulta final
  SELECT C1.especialidad 
    FROM (
      SELECT especialidad, COUNT(*)empleados 
        FROM medicos 
        GROUP BY especialidad
    ) C1 
    JOIN (
      SELECT MAX(C1.empleados) maximo 
        FROM (
          SELECT especialidad, COUNT(*)empleados 
            FROM medicos 
            GROUP BY especialidad
        ) C1
    ) C2 
    ON C1.empleados = C2.maximo;

-- 23. ¿Cuál es el nombre del hospital que tiene mayor número de plazas? 

  -- Subconsulta C1
  -- Maximo de plazas
  SELECT MAX(h.num_plazas)maximo 
    FROM hospitales h;

  -- Consulta final
  SELECT h.nombre 
    FROM hospitales h 
    WHERE h.num_plazas = (
      SELECT MAX(h.num_plazas)maximo 
        FROM hospitales h);

-- 24. Visualizar las diferentes estanterías de la tabla HERRAMIENTAS ordenados descendentemente por estantería. 

  SELECT DISTINCT h.estanteria 
    FROM herramientas h 
    ORDER BY h.estanteria DESC;

-- 25. Averiguar cuántas unidades tiene cada estantería. 

  SELECT h.estanteria, SUM(h.unidades)unidades 
    FROM herramientas h 
    GROUP BY h.estanteria;

-- 26. Visualizar las estanterías que tengan más de 15 unidades 

  SELECT h.estanteria, SUM(h.unidades)unidades 
    FROM herramientas h 
    GROUP BY h.estanteria
    HAVING unidades>15;

-- 27. ¿Cuál es la estantería que tiene más unidades? 

  -- C1 - unidades por estanteria
  SELECT h.estanteria, SUM(h.unidades)unidades 
    FROM herramientas h 
    GROUP BY h.estanteria;

  -- C2 - maximo unidades
  SELECT MAX(C1.unidades)maximo 
    FROM (
      SELECT h.estanteria, SUM(h.unidades)unidades 
        FROM herramientas h 
        GROUP BY h.estanteria
    ) C1;

  -- Consulta final 
  SELECT C1.estanteria 
    FROM (
      SELECT h.estanteria, SUM(h.unidades)unidades 
        FROM herramientas h 
        GROUP BY h.estanteria
    ) C1 
    JOIN (
      SELECT MAX(C1.unidades)maximo 
        FROM (
          SELECT h.estanteria, SUM(h.unidades)unidades 
            FROM herramientas h 
            GROUP BY h.estanteria
        ) C1
    ) C2 
    ON C1.unidades = C2.maximo;

-- 28. A partir de las tablas EMPLE y DEPART mostrar los datos del departamento que no tiene ningún empleado. 

  SELECT d.dept_no, d.dnombre, d.loc 
    FROM depart d 
    LEFT JOIN emple e 
    ON d.dept_no = e.dept_no 
    WHERE e.dept_no IS NULL;

-- 29. Mostrar el número de empleados de cada departamento. En la salida se debe mostrar también los departamentos que no tienen ningún empleado.

  SELECT d.dept_no, COUNT(e.dept_no)empleados 
    FROM depart d 
    LEFT JOIN emple e 
    ON d.dept_no = e.dept_no
    GROUP BY e.dept_no; 

-- 30. Obtener la suma de salarios de cada departamento, mostrando las columnas DEPT_NO, SUMA DE SALARIOS y DNOMBRE. En el resultado también se deben mostrar los departamentos que no tienen asignados empleados. 

  SELECT d.dept_no, SUM(e.salario) sumaSalarial, d.dnombre 
    FROM depart d 
    LEFT JOIN emple e 
    ON d.dept_no = e.dept_no 
    GROUP BY e.dept_no;

-- 31. Utilizar la función IFNULL en la consulta anterior para que en el caso de que un departamento no tenga empleados, aparezca como suma de salarios el valor 0. 

  SELECT d.dept_no, SUM(IFNULL(e.salario,0)) sumaSalarial, d.dnombre 
    FROM depart d 
    LEFT JOIN emple e 
    ON d.dept_no = e.dept_no   
    GROUP BY e.dept_no;

-- 32. Obtener el número de médicos que pertenecen a cada hospital, mostrando las columnas COD_HOSPITAL, NOMBRE y NÚMERO DE MÉDICOS. En el resultado deben aparecer también los datos de los hospitales que no tienen médicos.

  SELECT h.cod_hospital, h.nombre, COUNT(m.cod_hospital)numeroMedicos 
    FROM hospitales h 
    LEFT JOIN medicos m
    ON h.cod_hospital = m.cod_hospital
    GROUP BY m.cod_hospital;